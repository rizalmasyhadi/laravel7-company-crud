# COMPANY CRUD - LARAVEL 7

---

[Laravel 7](https://laravel.com/)

## Prerequisites

1. PHP >= 7.2.5
1. MySQL or MariaDB
1. [Composer](http://getcomposer.org)


## Getting Started

1. Clone to your base project directory.

	```
	git clone https://rizalmasyhadi@bitbucket.org/rizalmasyhadi/laravel7-company-crud.git
	```

2. Install vendor

	```
	composer install
	```

3. Add storage link to public

	```
	php artisan storage:link
	```

4. Create configuration file `.env` (copy from `.env.example`) and set the database configuration.

	```
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=laravel_company
	DB_USERNAME=root
	DB_PASSWORD=
	```

5. Database migration.

	```
	php artisan migrate
	```

6. Seed User

	```
	php artisan db:seed --class=UserTableSeeder
	```
	
7. For security reason, please generate new application key.

	```
	php artisan key:generate
	```
