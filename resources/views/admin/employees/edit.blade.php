<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="modal-edit-label" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-edit-label">Edit Company</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="form-edit" method="post" enctype="multipart/form-data" >
	          	  	@csrf
	          	  	@method('PUT')
	          	    
	          	    <div class="card-body">
	          	    	<div class="row">
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="first_name">First Name</label>
	          	    			  	<input type="text" class="form-control  @error("first_name") is-invalid @enderror" id="first_name" name="first_name" placeholder="Enter first name" value="{{ old('first_name') }}">
	          	    			  	@error("first_name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="last_name">Last Name</label>
	          	    			  	<input type="text" class="form-control  @error("last_name") is-invalid @enderror" id="last_name" name="last_name" placeholder="Enter last name" value="{{ old('last_name') }}">
	          	    			  	@error("last_name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="company_id">Company</label>
	          	    			  	<select name="company_id" id="company_id" class="form-control select2" style="width: 100%;">
		          	    			  	@foreach( $companies as $company )
		          	    			  		<option value="{{ $company->id }}">{{ $company->name }}</option>
		          	    			  	@endforeach
	          	    			  	</select>

	          	    			  	@error("company_id")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>

	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="email">Email</label>
	          	    			  	<input type="email" class="form-control  @error("email") is-invalid @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
	          	    			  	@error("email")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>

	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="phone">Phone</label>
	          	    			  	<input type="text" class="form-control  @error("phone") is-invalid @enderror" id="phone" name="phone" placeholder="Enter phone" value="{{ old('phone') }}">
	          	    			  	@error("phone")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    	</div>

	          	    	<input type="hidden" name="id" id="id" value="{{ old('id') }}">
	          	    </div>
	          	    <!-- /.card-body -->

	          	    <div class="card-footer">
	          	      <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
	          	    </div>

	          	</form>

			</div>

		</div>
	</div>
</div>