@extends('layouts.app', ['body_class' => 'hold-transition sidebar-mini layout-fixed'])
@php 
$employees_nav = 'active';
@endphp
@section('content')
<div class="wrapper">
    @include('admin.partials.navbar')
    @include('admin.partials.sidebar')
   
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0">Employees</h1>
                </div><!-- /.col -->
               
              </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="card">
	            <div class="card-header">
	                <a href="{{ route('admin.employees.create') }}" class="btn btn-primary">New Employee</a>
	            </div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="employee" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Index</th>
								<th>Full Name</th>
								<th>Company</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ( $employees as $employee )
								<tr>
									<td>{{ $employee->id }}</td>
									<td>{{ $employee->full_name }}</td>
									<td>
										<button type="button" class="btn btn-link btn-company"
											data-url="{{route('admin.companies.show', $employee->company->id )}}" >
											{{ $employee->company->name }}
										</button>
									</td>
									<td>
										<div class="d-flex">
											
											<button type="button" class="btn btn-sm btn-primary btn-edit" 
											
												data-url="{{route('admin.employees.show', $employee->id )}}"
												data-update="{{ route('admin.employees.update', [ 'id' => $employee->id ] )}}"
											 >
												Edit
											</button>

											<form action="{{ route('admin.employees.delete',$employee->id) }}" method="POST" onsubmit="return confirm('Are you sure you want delete this employee?')">
							                    @csrf
							                    @method('DELETE')
							                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
							                </form>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.card-body -->

            </div>
            <!-- /.card -->

        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>


@endsection

@section('modal')
@include('admin.employees.modal')
@include('admin.employees.edit')
@endsection
@push('scripts')
<script>
	$employees = @json($employees);
	$data = null;
	$(function () {

		$("#employee").DataTable({
			"responsive": true,
			"lengthChange": false,
			"autoWidth": false,

		})

		$('.btn-company').click(function(event) {
			let url = $(this).data('url');
			let update = $(this).data('update');
			
			getData('company', url)
				.then( ()  => {
					let html = `
						<tr>
							<th>Company Name</th>
							<td class="">`+( $data.name == null ? '' : $data.name  ) +`</td>
						</tr>
						<tr>
							<th>Email</th>
							<td class="">`+( $data.email == null ? '' : $data.email  ) +`</td>
						</tr>
						<tr>
							<th>Website</th>
							<td class="">`+ ( $data.website == null ? '' : $data.website  ) +`</td>
						</tr>
					`;
					$('#company-details').html(html);
				})
			
		});

		$('.btn-edit').click(function(event) {
			let url = $(this).data('url');
			let update = $(this).data('update');
			
			$('#form-edit').attr({
				action: update,
			});
			$('.invalid-feedback').remove();
			$('.is-invalid').removeClass('is-invalid');
			getData('edit', url, true)
			
		});

		@if ($errors->any())
		    validated();
		@endif

	});

	function validated(){
		let url = '{{ route('admin.employees.show', old('id', 0) ) }}';
		let update = '{{ route('admin.employees.update', old('id', 0)) }}';
		
		$('#form-edit').attr({
			action: update,
		});

		getData('edit', url)

	}
	

	async function getData(type, url, formSet = false){
		let getData = await fetch(url);
		let data = $data =  await getData.json();

		if(formSet){
			$('#first_name').val(data.first_name);
			$('#last_name').val(data.last_name);
			$('#company_id').val(data.company_id);
			$('#email').val(data.email);
			$('#phone').val(data.phone);
			$('#id').val(data.id);
			$('#email').val(data.email);
			$('#website').val(data.website);
		}

		if(data.logo){
			$('.pad').attr('src', '{{asset('storage')}}/'+data.logo);
		}

		$('#modal-'+type).modal('show')

	}

</script>
@endpush
