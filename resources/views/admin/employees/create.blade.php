@extends('layouts.app', ['body_class' => 'hold-transition sidebar-mini layout-fixed'])
@php 
$employees_nav = 'active';
@endphp
@section('content')
<div class="wrapper">
    @include('admin.partials.navbar')
    @include('admin.partials.sidebar')
   
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
					<a href="{{ route('admin.employees.index') }}" title="" class="btn btn-primary">Back</a>
                </div><!-- /.col -->
               
              </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          	<div class="card card-primary">
	          	<div class="card-header">
	          	    <h3 class="card-title">New Employee</h3>
	          	</div>
	          	<!-- /.card-header -->

	          	<!-- form start -->
	          	<form method="post" enctype="multipart/form-data" action="{{ route('admin.employees.store' ) }}" >
	          	  	@csrf
	          	    <div class="card-body">
	          	    	<div class="row">
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="first_name">First Name</label>
	          	    			  	<input type="text" class="form-control  @error("first_name") is-invalid @enderror" id="first_name" name="first_name" placeholder="Enter first name" value="{{ old('first_name') }}">
	          	    			  	@error("first_name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="last_name">Last Name</label>
	          	    			  	<input type="text" class="form-control  @error("last_name") is-invalid @enderror" id="last_name" name="last_name" placeholder="Enter last name" value="{{ old('last_name') }}">
	          	    			  	@error("last_name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="company_id">Company</label>
	          	    			  	<select name="company_id" id="company_id" class="form-control select2" style="width: 100%;">
		          	    			  	@foreach( $companies as $company )
		          	    			  		<option value="{{ $company->id }}">{{ $company->name }}</option>

		          	    			  	@endforeach
	          	    			  	</select>

	          	    			  	@error("company_id")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>

	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="email">Email</label>
	          	    			  	<input type="email" class="form-control  @error("email") is-invalid @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
	          	    			  	@error("email")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>

	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="phone">Phone</label>
	          	    			  	<input type="text" class="form-control  @error("phone") is-invalid @enderror" id="phone" name="phone" placeholder="Enter phone" value="{{ old('phone') }}">
	          	    			  	@error("phone")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    	</div>

	          	    </div>
	          	    <!-- /.card-body -->

	          	    <div class="card-footer">
	          	      <button type="submit" class="btn btn-primary btn-block">Save</button>
	          	    </div>
	          	</form>
          	</div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
@endsection
