<div class="modal fade" id="modal-company" tabindex="-1" aria-labelledby="modal-company-label" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-company-label">Company</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body p-0" id="modal-body-company">
				
				<div class="card card-widget">

		            <div class="card-body">
		                <img class="img-fluid pad" src="../dist/img/photo2.png" alt="Photo">

		                <table class="table table-borderless">

		                	<tbody id="company-details">
		                		<tr>
		                			<th>Company Name</th>
		                			<td class="">Company Name</td>
		                		</tr>
		                		<tr>
		                			<th>Email</th>
		                			<td>Company Name</td>
		                		</tr>
		                		<tr>
		                			<th>Website</th>
		                			<td>Company Name</td>
		                		</tr>
		                		<tr>
		                			<th>Website</th>
		                			<td>Company Name</td>
		                		</tr>

		                	</tbody>
		                </table>

		            </div>
	              	<!-- /.card-body -->


	            </div>
			</div>

		</div>
	</div>
</div>