@extends('layouts.app', ['body_class' => 'hold-transition sidebar-mini layout-fixed'])
@php 
$companies_nav = 'active';
@endphp
@section('content')
<div class="wrapper">
    @include('admin.partials.navbar')
    @include('admin.partials.sidebar')
   
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
					<a href="{{ route('admin.companies.index') }}" title="" class="btn btn-primary">Back</a>
                </div><!-- /.col -->
               
              </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          	<div class="card card-primary">
	          	<div class="card-header">
	          	    <h3 class="card-title">New Company</h3>
	          	</div>
	          	<!-- /.card-header -->

	          	<!-- form start -->
	          	<form method="post" enctype="multipart/form-data" action="{{ route('admin.companies.store' ) }}" >
	          	  	@csrf
	          	    <div class="card-body">
	          	    	<div class="row">
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="name">Name</label>
	          	    			  	<input type="name" class="form-control  @error("name") is-invalid @enderror" id="name" name="name" placeholder="Enter name" value="{{ old('name') }}">
	          	    			  	@error("name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="email">Email</label>
	          	    			  	<input type="email" class="form-control  @error("email") is-invalid @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
	          	    			  	@error("email")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
		          	    			<label for="logo">Logo</label>
		          	    			<div class="input-group">
		          	    			    <input type="file" class="form-control @error("logo") is-invalid @enderror" name="logo"  id="logo" 
		          	    			      	onchange="function showPict(e){
				                                var file = e.target.files[0];
				                                var fileReader = new FileReader();
				                                fileReader.onload = function(e) { 
				                                    $('#logo-prev').attr('src',fileReader.result )
				                                };
				                                fileReader.readAsDataURL(file);
				                            } showPict(event)"
		          	    			      >
		          	    			      <label class="custom-file-label" for="logo">Choose file</label>
		          	    			</div>
		          	    			@error("logo")
		          	    			    <span class="invalid-feedback" role="alert">
		          	    			        <strong>{{ $message }}</strong>
		          	    			    </span>
		          	    			@enderror
	          	    			</div>
	          	    			<div class="col-md-12 text-center">
	          	    			    <img id="logo-prev" style="height: 30vh" src="{{ asset('images/noimage.png') }}" />
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="website">Website</label>
	          	    			  	<input type="text" class="form-control  @error("website") is-invalid @enderror" id="website" name="website" value="{{ old('website') }}" placeholder="Enter Website">
	          	    			  	@error("website")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    	</div>

	          	    </div>
	          	    <!-- /.card-body -->

	          	    <div class="card-footer">
	          	      <button type="submit" class="btn btn-primary btn-block">Save</button>
	          	    </div>
	          	</form>
          	</div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
@endsection
