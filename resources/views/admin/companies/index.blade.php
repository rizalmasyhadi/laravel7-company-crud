@extends('layouts.app', ['body_class' => 'hold-transition sidebar-mini layout-fixed'])
@php 
$companies_nav = 'active';
@endphp
@section('content')
<div class="wrapper">
    @include('admin.partials.navbar')
    @include('admin.partials.sidebar')
   
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0">Companies</h1>
                </div><!-- /.col -->
               
              </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="card">
	            <div class="card-header">
	                <a href="{{ route('admin.companies.create') }}" class="btn btn-primary">New Company</a>
	            </div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="company" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Index</th>
								<th>Name</th>
								<th>Email</th>
								<th>Logo</th>
								<th>Website</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ( $companies as $company )
								<tr>
									<td>{{ $company->id }}</td>
									<td>{{ $company->name }}</td>
									<td>{{ $company->email }}</td>
									<td>
										@if( $company->logo)
											<img src="{{ asset('storage/'.$company->logo) }}" alt="" style="object-fit: cover;height: 100px;width: 100px;">
										@endif
									</td>
									<td>
										@if($company->website)
											<a class="btn-link" href="{{ $company->website }}" title="" target="_blank">{{ $company->website }}</a>
										@endif
									</td>
									<td>
										<div class="d-flex">
											
											<button type="button" class="btn btn-sm btn-primary btn-edit" 
											
												data-url="{{route('admin.companies.show', $company->id )}}"
												data-update="{{ route('admin.companies.update', [ 'id' => $company->id ] )}}"
											 >
												Edit
											</button>
											<form action="{{ route('admin.companies.delete',$company->id) }}" method="POST" onsubmit="return confirm('Are you sure you want delete this company?')">
							                    @csrf
							                    @method('DELETE')
							                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
							                </form>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.card-body -->

            </div>
            <!-- /.card -->

        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>

@endsection

@section('modal')
@include('admin.companies.edit')
@endsection
@push('scripts')
<script>
	$companies = @json($companies);

	$(function () {

		$("#company").DataTable({
			"responsive": true,
			"lengthChange": false,
			"autoWidth": false,

		})

		$('.btn-edit').click(function(event) {
			let url = $(this).data('url');
			let update = $(this).data('update');
			
			$('#form-edit').attr({
				action: update,
			});
			$('.invalid-feedback').remove();
			$('.is-invalid').removeClass('is-invalid');
			getCompanyData(url, true);
			
		});

		@if ($errors->any())
		    validated();
		@endif

	});

	function validated(){
		let url = '{{ route('admin.companies.show', old('id', 0) ) }}';
		let update = '{{ route('admin.companies.update', old('id', 0)) }}';
		
		$('#form-edit').attr({
			action: update,
		});

		getCompanyData(url);

	}
	

	async function getCompanyData(url, formSet = false){
		let getData = await fetch(url);
		let data = await getData.json();
		if(formSet){

			$('#name').val(data.name);
			$('#id').val(data.id);
			$('#email').val(data.email);
			$('#website').val(data.website);
		}
		if(data.logo){
			$('#logo-prev').attr('src', '{{asset('storage')}}/'+data.logo);
		}

		$('#modal-edit').modal('show')

	}

</script>
@endpush
