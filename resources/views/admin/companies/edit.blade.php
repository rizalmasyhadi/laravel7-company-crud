<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="modal-edit-label" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-edit-label">Edit Company</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="form-edit" method="post" enctype="multipart/form-data" >
	          	  	@csrf
	          	  	@method('PUT')
	          	    <div class="card-body">
	          	    	<div class="row">
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="name">Name</label>
	          	    			  	<input type="name" class="form-control  @error("name") is-invalid @enderror" id="name" name="name" placeholder="Enter name" value="{{ old('name') }}">
	          	    			  	@error("name")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="email">Email</label>
	          	    			  	<input type="email" class="form-control  @error("email") is-invalid @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
	          	    			  	@error("email")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
		          	    			<label for="logo">Logo</label>
		          	    			<div class="input-group">
		          	    			    <input type="file" class="form-control @error("logo") is-invalid @enderror" name="logo"  id="logo" 
		          	    			      	onchange="function showPict(e){
				                                var file = e.target.files[0];
				                                var fileReader = new FileReader();
				                                fileReader.onload = function(e) { 
				                                    $('#logo-prev').attr('src',fileReader.result )
				                                };
				                                fileReader.readAsDataURL(file);
				                            } showPict(event)"
		          	    			      >
		          	    			      <label class="custom-file-label" for="logo">Choose file</label>
		          	    			</div>
		          	    			@error("logo")
		          	    			    <span class="invalid-feedback" role="alert">
		          	    			        <strong>{{ $message }}</strong>
		          	    			    </span>
		          	    			@enderror
	          	    			</div>
	          	    			<div class="col-md-12 text-center">
	          	    			    <img id="logo-prev" style="height: 30vh" src="{{ asset( old('logo') ? 'storage/'.old('logo') : 'images/noimage.png' ) }}" />
	          	    			</div>
	          	    		</div>
	          	    		<div class="col-6">
	          	    			<div class="form-group">
	          	    			  	<label for="website">Website</label>
	          	    			  	<input type="text" class="form-control  @error("website") is-invalid @enderror" id="website" name="website" value="{{ old('website') }}" placeholder="Enter Website">
	          	    			  	@error("website")
	          	    			  	    <span class="invalid-feedback" role="alert">
	          	    			  	        <strong>{{ $message }}</strong>
	          	    			  	    </span>
	          	    			  	@enderror
	          	    			</div>
	          	    		</div>
	          	    	</div>

	          	    	<input type="hidden" name="id" id="id" value="{{old('id')}}">

	          	    </div>
	          	    <!-- /.card-body -->

	          	    <div class="card-footer">
	          	      <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
	          	    </div>

	          	</form>

			</div>

		</div>
	</div>
</div>