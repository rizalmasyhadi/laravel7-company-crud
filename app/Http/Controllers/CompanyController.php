<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Session;
    		
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function index(){
    	$companies = Company::all();
    	return view('admin.companies.index', compact('companies'));
    }

    public function create(){
    	return view('admin.companies.create');
    }

    public function store(Request $request){
    	$validate = $request->validate([
	        'name' => 'required|unique:companies|max:100',
	        'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);

    	$data = $request->except('logo', '_token');

        if ($request->hasFile('logo')) {

			$logo = $request->file('logo');
			$name = $logo->hashName();
			$extension = $logo->extension();
			$path = $request->file('logo')->storeAs('/', $name.".{$extension}", 'public');
			$data['logo'] = $path;
        }

        $company = Company::create($data);

    	return redirect()->route('admin.companies.index')->with('success', 'New company successfully saved');
    }

    public function show($id){
    	if( $company = Company::find($id) ){
    		return response()->json($company, 200);
    	}else{
    		return response()->json('no data found', 404);
    	}
    }

    public function update($id, Request $request){

    	if(!$company = Company::find($id)){
    		abort(404);
    	}

    	$validate = $request->validate([
	        'name' => 'required|max:100|unique:companies,name,'.$id,
	        'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);

	    $data = $request->except('logo', '_token', 'url');

        if ($request->hasFile('logo')) {
			$logo = $request->file('logo');
			$name = $logo->hashName();
			$extension = $logo->extension();
			$path = $request->file('logo')->storeAs('/', $name.".{$extension}", 'public');
			$data['logo'] = $path;
        }

        $company->update($data);
    	return redirect()->route('admin.companies.index')->with('success', 'Company successfully updated');
    }

    public function destroy($id){

    	if($company = Company::find($id)){
    		Storage::delete($company->logo);
    		$company->delete();
    		return redirect()->route('admin.companies.index')->with('success', 'Company successfully deleted');
    	}

    	return redirect()->back()->withErrors(['message'=>'No company found']);

    }

}
