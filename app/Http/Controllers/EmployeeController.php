<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Employee;
use Session;
    		
class EmployeeController extends Controller
{
    public function index(){
    	$companies = Company::all();
    	$employees = Employee::whereHas('company')->get();
    	return view('admin.employees.index', compact('employees', 'companies'));
    }

    public function create(){
    	$companies = Company::all();
    	return view('admin.employees.create', compact('companies'));
    }

    public function store(Request $request){
    	$validate = $request->validate([
	        'first_name' => 'required|max:100',
	        'last_name' => 'required|max:100',
	        'company_id' => 'required|numeric',
	    ]);

    	$data = $request->except('_token');

        $employee = Employee::create($data);

    	return redirect()->route('admin.employees.index')->with('success', 'New employee successfully saved');
    }

    public function show($id){
    	if( $employee = Employee::find($id) ){
    		return response()->json($employee, 200);
    	}else{
    		return response()->json('no data found', 404);
    	}
    }

    public function update($id, Request $request){

    	if(!$employee = Employee::find($id)){
    		abort(404);
    	}

    	$validate = $request->validate([
	        'first_name' => 'required|max:100',
	        'last_name' => 'required|max:100',
	        'company_id' => 'required|numeric',
	    ]);

    	$data = $request->except('_token');
        $employee->update($data);

    	return redirect()->route('admin.employees.index')->with('success', 'Employee successfully updated');
    }

    public function destroy($id){

    	if($employee = Employee::find($id)){
    		$employee->delete();
    		return redirect()->route('admin.employees.index')->with('success', 'Employee successfully deleted');
    	}

    	return redirect()->back()->withErrors(['message'=>'No employee found']);

    }
}
