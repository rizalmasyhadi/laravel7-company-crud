<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes([
	'register' => false,
	'reset' => false,
	'verify' => false,
]);

Route::group(['prefix' => 'admin', 'as' => 'admin' , 'middleware' => ['auth','check.user'] ], function() {

    Route::get('/', function(){
    	return view('admin.index');
    });

    Route::group(['prefix' => 'companies',  'as' => '.companies' ], function() {
        Route::get('/', 'CompanyController@index')->name('.index');
        Route::get('/create', 'CompanyController@create')->name('.create');
        Route::post('/store', 'CompanyController@store')->name('.store');
        Route::get('/show/{id}', 'CompanyController@show')->name('.show');
        Route::put('/update/{id}', 'CompanyController@update')->name('.update');
        Route::delete('/delete/{id}', 'CompanyController@destroy')->name('.delete');
    });


    Route::group(['prefix' => 'employees',  'as' => '.employees' ], function() {
        Route::get('/', 'EmployeeController@index')->name('.index');
        Route::get('/create', 'EmployeeController@create')->name('.create');
        Route::post('/store', 'EmployeeController@store')->name('.store');
        Route::get('/show/{id}', 'EmployeeController@show')->name('.show');
        Route::put('/update/{id}', 'EmployeeController@update')->name('.update');
        Route::delete('/delete/{id}', 'EmployeeController@destroy')->name('.delete');
    });
    
});

