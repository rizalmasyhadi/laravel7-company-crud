<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$newUsers = [
    		[
    			'name' => 'admin',
    			'email' => 'admin@grtech.com.my',
    			'password' => Hash::make('password'),
    		],
    		[
    			'name' => 'user',
    			'email' => 'user@grtech.com.my',
    			'password' => Hash::make('password'),
    		],
    	];

        DB::table('users')->insert($newUsers);
    }
}
